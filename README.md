# wherethefuckisxur-script
---
### Note:
wherethefuckisxur-script and -server accomplishes two different things. -script talks to Bungie API and writes data into storage/, while -server uses the data and renders the website. [Check out the -server repository for more information](https://gitlab.com/kingsofxur/wherethefuckisxur-server).

# Getting Started
This project requires node.js, so figure out how to install that if you haven't already.
First, clone yourself a version of this repository.

    git clone https://gitlab.com/kingsofxur/wherethefuckisxur-script.git
    
Inside the repo, run `npm i` to install all required node packages.

    npm i
    
The script looks for storage/config.json, so create the storage directory and also config.json
    
    mkdir storage && cd storage
    vim config.json

There's a few things that need to be in config.json. Here's what your config.json needs to look like:
    
    {
      "discordbot": "Your discord token",
      "token": {
        "refresh": "Your bungie refresh token",
        "expires": "999999999999999999"
      },
      "api": {
        "key": "Your bungie API key",
        "id": "Your app's client id",
        "secret": "Your bungie API secret"
      },
      "charinfo": {
        "platform": "4",
        "membershipid": "Your profile's membershipid",
        "charid": "One of your character's id"
      }
    }
    
Some quick explanations before I explain how to grab these values.
* -script uses a Discord bot to update xur information, so you will need to make yourself one for your sandbox. 
* You will need to get access to Bungie's API (and create your Bungie app) for a few things:
    * Refresh token is a token that shows Bungie you've authorized your account with your Bungie app. You will only need to grab this once, as it takes like 3 months for the refresh token to expire.
    * API key is your Bungie app's API key, client id is your app's client id, secret is your bungie app's secret key. Self explanatory
    * As for Charinfo, you will use your own profile and character to call Bungie API. **Platform** values are "1" for Xbox, "2" for Playstation, and "4" for Blizzard (PC).
* Make sure that your values are all strings, even the numbers and tokens. I've had the script truncate my IDs before (blame Javascript).

##### Creating a Discord Bot.
1. Head over to https://discordapp.com/developers/applications/
2. Click on `New Application`, name your application. Jot down the `Client ID` in that screen.
3. Click on `Bot` on the left sidebar. Then `Add Bot`. Put that `Token` provided in that screen in your `config.json` (warning! Keep your token private, it gives other people access to your bot!).
4. Add your bot to your private sandbox discord server (or wherever, I'm not your mom).
    * To add a bot, you need to be logged into discord and then go to your bot's invite link. The format is 

            https://discordapp.com/oauth2/authorize?client_id=YOUR_CLIENT_ID&scope=bot&permissions=YOUR_PERMISSION_VALUE

    * To grab the permission value, in the bot page, scroll down to where it says permission, and check whichever permission your bot needs. At the bare minimum, it needs "Send Messages", "Read Message History", and "Add Reactions", so the value would be `67648` (I could be wrong about only needing those three. You could just give your bot every permission, it's your bot after all.`8` for "Administrator").
    
##### Bungie API
1. Head over to https://www.bungie.net/en/Application and hit `Create New App`.
2. **Important**: Set your `OAuth Client Type` to "Confidential". If you don't you won't find a secret key later.
3. I'm not super sure about the `Scope` you need to set, so I'd recommend just setting all of them. I *think* at the bare minimum you need "Read your Destiny vendor and advisor information." and "Access items like your notifications, memberships, and recent activity." and *maybe* "Read your Destiny vault and character inventory."
4. Set `Origin Header` to "*" (asterisk). Then finish creating the app.
5. Head to your Bungie App's page. Put `API Key`, `OAuth client_id`, and `OAuth client_secret` in your `config.json` (Again, API key and client_secret should be kept private.) (**If you can't find your client_secret, make sure your client type is set to confidential**).
6. Scroll down a bit and for `Redirect URL`, entering literally anything that looks like a qualified link. (For example, mine is https://klasdfkasdhfkasdjhf.com. It's not super important what you enter, it's relevant once for you to get your authorization code).
7. Go to your OAuth link, the format is:

        https://www.bungie.net/en/OAuth/Authorize?client_id=YOUR_CLIENT_ID&response_type=code
        
8. Go through and allow your account authorization to your app. This basically allows your app to look at your Bungie account's stuffs.
9. Once you finish, you should be redirected to something that looks like 

        https://klasdfkasdhfkasdjhf.com/?code=JOT_DOWN_THIS_CODE
        
10. Yeah, jot that `code` down.
11. We're going to use that code to make an HTTP POST request to Bungie's API to get a refresh token. To do this with Curl:

        curl -X POST -H 'X-API-KEY: YOUR_API_KEY' \
        -d 'client_id=YOUR_CLIENT_ID&client_secret=YOUR_CLIENT_SECRET&grant_type=authorization_code&code=YOUR_CODE_THAT_YOU_JOT_DOWN_EARLIER' \
        'https://www.bungie.net/platform/app/oauth/token/'

    * Some notes if you want to use other methods, Bungie wants you to use the x-www-form-urlencoded format. Curl does it for you automagically but you might have to flip some switches for other tools. Also if you're not familiar with curl, -H specifies header, -d specifies POST data, and the last argument is the request URL.
    * Also note that Bungie API cares about trailing '/', so make sure you be mindful about whether I have the trailing '/' in my URLs.
    
12. If you're lucky, Bungo will give you back a JSON containing some important tokens. The one we care about is the `access_token` and `refresh_token`. Put the `refresh_token` in your `config.json`. The `access_token` is needed to grab your destiny membership id and character id.
13. Another curl command to grab your destiny membership id:

        curl -X GET -H 'X-API-KEY: YOUR_API_KEY' \
        -H 'Authorization: Bearer YOUR_ACCESS_TOKEN' \
        'https://www.bungie.net/Platform/User/GetMembershipsForCurrentUser/'

14. Grab the `membershipId` inside the "destinyMemberships" field and put it in your `config.json`. **Important**: There are two different membershipIDs, don't be confused. One is destinyMembership id, and the other is your Bungie account membership ID. Make sure you grab the destiny one.
15. With your destiny membership id in hand, make another curl to get your character id:
 
        curl -X GET -H 'X-API-KEY: YOUR_API_KEY' \
        'https://www.bungie.net/Platform/Destiny2/4/Profile/YOUR_DESTINY_MEMBERSHIP_ID/?components=200'

    * That '/' before the '?' is on purpose. Idk why but bungo hates it when I make a curl without it.
    
16. Grab the first `characterId` you find and put it in `config.json`. You should be able to sort of verify which character it is by looking at your light level and classType (0 = Titan, 1 = Hunter, 2 = Warlock). I don't think it matters which characterId you use.
17. Ok cool, by now you should have gotten your `config.json` all filled out. Now run

        npm start
    
    to run the script. If you did it right, you should start seeing running bungiefetch and done! You should also see new files popping into storage.json.
    
# Xurbot command
These are bot commands invoked through Discord.
`$xur tower|titan|io|nessus|edz` - If xur's here, update data on xur's location.
`$xurmsg message` - Update the xur message (the one that's right below the bold text on where xur is).
`$psa message` - Update the PSA message (the one that's under "Public Xûrvice Announcement", to the right of xur location bold text).
`$riff message` - Update the Thought of The Day (the one that's right below vendor cards).