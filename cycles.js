function calculateCycles() {
    let cycles = {};

    let firstResetTime = 1539709200;
    let currentTime = Math.floor((new Date()).getTime() / 1000);
    let secondsSinceFirst = currentTime - firstResetTime;
    let daysSinceFirst = Math.floor(secondsSinceFirst / 86400);
    let weeksSinceFirst = Math.floor(secondsSinceFirst / 604800);

    let challenges = ['Ouroborea', 'Forfeit Shrine', 'Shattered Ruins', 'Keep of Honed Edges', 'Agonarch Abyss', 'Cimmerian Garrison'];
    let challengeLocs = ['Aphelion\'s Rest > floating around in the main area', 'Gardens of Esila > off the cliff near the entrance', 'Spine of Keres > above a rock in the purple fog area', 'Harbinger\'s Seclude > on top of the statue in back of the area', 'Bay of Drowned Wishes > in the bay area, just off the main path', 'Chamber of Starlight > on the cliff behind the chest']
    let wellbosses = ['Sikariis and Varkuuriis, Plagues of the Well', 'Cragur, Plague of the Well', 'Inomia, Plague of the Well'];
    let citystatuses = ['Strongest', 'Weakest', 'Growing'];
    let eclipsedzone = ['Asterion Abyss', 'Cadmus Ridge', 'Eventide Ruins'];
    let legendlostsector = ['Chamber of Starlight', 'Aphelion\'s Rest', 'The Empty Tank', 'K1 Logistics', 'K1 Communion', 'K1 Crew Quarters', 'K1 Revalation', 'Concealed Void', 'Bunker E15', 'Perdition', 'Bay of Drowned Wishes'];
    let masterlostsector = ['Bay of Drowned Wishes', 'Chamber of Starlight', 'Aphelion\'s Rest', 'The Empty Tank', 'K1 Logistics', 'K1 Communion', 'K1 Crew Quarters', 'K1 Revalation', 'Concealed Void', 'Bunker E15', 'Perdition'];
    let legendlostsectorslot = ['Head', 'Legs', 'Arms', 'Chest'];
    let masterlostsectorslot = ['Chest', 'Head', 'Legs', 'Arms'];
    let ordealloot = ['The Palindrome, The SWARM', 'Shadow Price, The Comedian', 'The Hothead, Hung Jury SR4', 'The Comedian, Uzume RR4'];

    cycles.ascendantchallenge = {
        name: challenges[weeksSinceFirst % challenges.length],
        location: challengeLocs[weeksSinceFirst % challengeLocs.length],
        id: weeksSinceFirst % challenges.length
    }

    cycles.citystatus = {
        status: citystatuses[weeksSinceFirst % citystatuses.length],
        boss: wellbosses[weeksSinceFirst % wellbosses.length],
        id: weeksSinceFirst % citystatuses.length
    }

    cycles.eclipsedzone = {
        zone: eclipsedzone[weeksSinceFirst % eclipsedzone.length]
    }

    cycles.lostsectors = {
        legendsector: legendlostsector[daysSinceFirst % legendlostsector.length],
        legendloot: legendlostsectorslot[daysSinceFirst % legendlostsectorslot.length],
        mastersector: masterlostsector[daysSinceFirst % masterlostsector.length],
        masterloot: masterlostsectorslot[daysSinceFirst % masterlostsectorslot.length]
    }

    cycles.ordealloot = {
        gun: ordealloot[weeksSinceFirst % ordealloot.length]
    }

    return cycles;
}

module.exports = { calculateCycles };
