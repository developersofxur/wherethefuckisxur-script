FROM node:16.14-alpine

WORKDIR /app/

COPY package.json package-lock.json /app/
RUN npm i

COPY . /app/

CMD npm start